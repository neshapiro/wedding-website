Rails.application.routes.draw do
  get 'sessions/new'
  get 'static_pages/hotel'
  get 'static_pages/menu'

  root 'static_pages#home'
  get '/photos', to: 'static_pages#photos'
  get '/registries', to: 'static_pages#registries'
  get '/info', to: 'static_pages#hotel'
  get '/menu', to: 'static_pages#menu'
  get '/register', to: 'static_pages#register'
  post '/register', to: 'users#create'
  post 'guests/:id/rsvp', to: 'users#edit'
  get '/guestlist', to: 'users#guestlist'
  get '/schedule', to: 'static_pages#schedule'
  get '/confirmrsvp', to: 'static_pages#rsvp'
  get '/decline', to: 'static_pages#decline'
  post '/decline', to: 'static_pages#decline'
  get '/inviteall', to: 'users#invite_all_guests'
  
  resources :users, :path => "guests", path_names: { edit: 'rsvp', edit_registerform: 'register' } do
    member do
      get :edit_registerform
      put :update_registerform
      get :invitation
      get :decline
      get :accept
      get :send_invitation_email
    end
  end
  
  # resources :users, :path => "guests", path_names: { edit: 'update' }
  
  
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
