json.array!(@users) do |user|
  json.extract! user, :id, :name, :email, :rsvp, :plus_one, :veg, :plus_one_veg, :registernote, :invitationoptout
  json.url user_url(user, format: :json)
end
