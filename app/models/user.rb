class User < ApplicationRecord
  before_save {self.email = email.downcase }
  before_save {self.name = name.titleize}
  validates :name, presence: true, length: {maximum: 50 }, uniqueness: {case_sensitive: false }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }, uniqueness: {case_sensitive: false }
  # validates :registernote, presence: true, length: { maximum: 500 }
  # validates :guestone, uniqueness: {case_sensitive: false }
  # validates :guesttwo, uniqueness: {case_sensitive: false }
  # validates :guestthree, uniqueness: {case_sensitive: false }
  # validates :guestfour, uniqueness: {case_sensitive: false }
  # validates :guestfive, uniqueness: {case_sensitive: false }
end
