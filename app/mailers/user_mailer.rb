class UserMailer < ApplicationMailer
  default from: "donotreply@shapirowedding.com"
  layout 'mailer'

  def new_user(user)
    @user = user
    mail to: user.email, subject: "Guest Registration - Shapiro Wedding"
  end
  
  def new_self(user)
    @user = user
    email = "nshapiro91@gmail.com; tcsmiddy12@gmail.com"
    mail(to: email, subject: "#{user.name} has Registered")
  end
  
  def update_user(user)
    @user = user
    mail to: user.email, subject: "Guest Registration Update - Shapiro Wedding"
  end
  
  def update_self(user)
    @user = user
    email = "nshapiro91@gmail.com; tcsmiddy12@gmail.com"
    mail(to: email, subject: "#{user.name} has updated their information")
  end
  
  def invitation(user)
    @user = user
    attachments.inline['wedding_invitation_opt2.png'] = File.read('app/assets/images/wedding_invitation_opt2.png')
    mail to: user.email, subject: 'ACTION REQUIRED BY MARCH 1ST: Presence requested at the Shapiro Wedding!'
  end
  
  def rsvp_user(user)
    @user = user
    mail to: user.email, subject: 'Guest RSVP Received - Shapiro Wedding'
  end
  
  def rsvp_self(user)
    @user = user
    mail to: 'nshapiro91@gmail.com; tcsmiddy12@gmail.com', subject: "#{user.name} has submitted their RSVP"
  end
  
end