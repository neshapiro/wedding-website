class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy, :invitation]
  # before_action :logged_in_user, only: [:index]
  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end
  
  def guestlist
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end
  
  def edit_registerform
    @user = User.find(params[:id])
  end
  
  def update_registerform
    @user = User.find(params[:id])
  end
  
  def invitation
    @user = User.find(params[:id])
  end
  
  def send_invitation_email
    @user = User.find(params[:id])
    UserMailer.invitation(@user).deliver_later
    
    redirect_to guestlist_path
  end
  
  def invite_all_guests
    @users = User.all
    @users.each do |user|
      UserMailer.invitation(user).deliver_later
    end
    redirect_to guestlist_path
  end
  
  
  def accept
    @user = User.find(params[:id])
    @user.rsvp = true
    @user.save
    redirect_to edit_user_path(@user)
  end
  
  def decline
    @user = User.find(params[:id])
    @user.rsvp = false
    @user.save
    UserMailer.rsvp_user(@user).deliver_later
    UserMailer.rsvp_self(@user).deliver_later
    redirect_to decline_path
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        UserMailer.invitation(@user).deliver_later
        UserMailer.new_self(@user).deliver_later
        flash[:success] = "You are now registered! Check your email for confirmation!"
        format.html { redirect_to guestlist_path }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      # puts '---------------------------------------------------------'
      # puts @user
      # puts user_params
      # puts format
      # puts '---------------------------------------------------------'
      # puts @user.test
      if @user.update(user_params)
          if @user.rsvp?
            UserMailer.rsvp_user(@user).deliver_later
            UserMailer.rsvp_self(@user).deliver_later
          else
          end
          flash[:success] = "Your RSVP has been submitted, check your email for confirmation!"
          format.html { redirect_to @user }
          format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      # params.require(:user).permit(:name, :email, :registernote, :invitationoptout, :rsvp, :plus_one, :veg, :plus_one_veg)
      params.require(:user).permit(:name, :email, :registernote, :invitationoptout, :numguests, :invitationid, :allergies, :suggestedsong, :veg, :guestone, :guesttwo, :guestthree, :guestfour, :guestfive)
    end
end
