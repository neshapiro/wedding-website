class StaticPagesController < ApplicationController
  def home
  end
  
  def contact
  end
  
  def registries
  end
  
  def register
  end
  
  def photos
  end

  
  def hotel
  end

  def menu
  end
  
  def schedule
  end
  
  def rsvp
  end
  
  def decline
  end
  
end
