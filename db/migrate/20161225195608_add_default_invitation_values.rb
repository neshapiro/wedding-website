class AddDefaultInvitationValues < ActiveRecord::Migration[5.0]
  def up
    change_column_default :users, :invitationid, 0
    change_column_default :users, :numguests, 1
  end
  
  def down
    change_column_default :users, :invitationid, nil
    change_column_default :users, :numguests, nil
  end
end
