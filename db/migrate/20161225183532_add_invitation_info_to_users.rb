class AddInvitationInfoToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :invitationid, :integer
    add_column :users, :numguests, :integer
  end
end
