class AddRegisterFieldsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :registernote, :string
    add_column :users, :invitationoptout, :boolean
  end
end
