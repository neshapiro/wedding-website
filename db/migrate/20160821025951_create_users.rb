class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      # t.string :registernote
      # t.boolean :invitationoptout
      
      #t.boolean :rsvp
      #t.boolean :plus_one
      #t.boolean :veg
      #t.boolean :plus_one_veg

      t.timestamps
    end
  end
end
