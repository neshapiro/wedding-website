require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @base_title = "Shapiro Wedding"
  end
  
  test "should get root" do
    get root_path
    assert_response :success
  end
  
  test "should get photos" do
    get photos_path
    assert_response :success
    assert_select "title", "Photos | #{@base_title}"
  end

  test "should get registries" do
    get registries_path
    assert_response :success
    assert_select "title", "Registries | #{@base_title}"
  end
  
  test "should get contact" do
    get contact_path
    assert_response :success
    assert_select "title", "Contact | #{@base_title}"
  end

end
