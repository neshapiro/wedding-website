# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview
  
  def new_user_mail_preview
    user = User.first
    UserMailer.new_user(user)
  end
  
  def update_user_mail_preview
    user = User.first
    UserMailer.update_user(user)
  end
  
  def update_self_mail_preview
    user = User.first
    UserMailer.update_self(user)
  end
  
  def invitation_user_mail_preview
    user = User.first
    UserMailer.invitation(user)
  end

  def new_user_mail_preview
    user = User.first
    UserMailer.new_user(user)
  end
  
  def new_self_mail_preview
    user = User.first
    UserMailer.new_self(user)
  end
  
  
  def rsvp_self_mail_preview
    user = User.first
    UserMailer.rsvp_self(user)
  end
  
  def rsvp_user_mail_preview
    user = User.first
    UserMailer.rsvp_user(user)
  end
  
end
