var main = function () {
    $('.arrow-next').click(function () {
        var currentSlide = $('.active-slide');
        var nextSlide = currentSlide.next();

        var currentDot = $('.active-dot');
        var nextDot = currentDot.next();

        if (nextSlide.length === 0) {
            nextSlide = $('.slide').first();
            nextDot = $('.dot').first();
        }

        currentSlide.fadeOut(600).removeClass('active-slide');
        nextSlide.fadeIn(600).addClass('active-slide');

        currentDot.removeClass('active-dot');
        nextDot.addClass('active-dot');
    })

    $('.arrow-prev').click(function () {
        var currentSlide = $('.active-slide');
        var prevSlide = currentSlide.prev();

        var currentDot = $('.active-dot');
        var prevDot = currentDot.prev();

        if (prevSlide.length === 0) {
            prevSlide = $('.slide').last();
            prevDot = $('.dot').last();
        }

        currentSlide.fadeOut(600).removeClass('active-slide');
        prevSlide.fadeIn(600).addClass('active-slide');

        currentDot.removeClass('active-dot');
        prevDot.addClass('active-dot');
    });
    
    $(window).on('keydown', function(key){if(key.originalEvent.code !== "ArrowRight"){return;} $('.arrow-next').trigger('click');})
    $(window).on('keydown', function(key){if(key.originalEvent.code !== "ArrowLeft"){return;} $('.arrow-prev').trigger('click');})
    // $(image).on('keydown', function(key){ $('.arrow-prev').trigger('click');})
    // $(window).on('click', function(click){ $('.arrow-next').trigger('click');})
    // $(window).on('swiperight', function(){
    //   $('.arrow-next').trigger('click');
    // });
    // // $(window).on('click', function(click){
    //     $('.arrow-next').trigger('click');
    // });
};



$(document).ready(main);